# -*- coding: utf-8 -*-
import scrapy
from ..items import a55dyItem


class A55dySpider(scrapy.Spider):
    name = '55dy'
    allowed_domains = ['www.55cc.cc']
    start_urls = ['http://www.55cc.cc/']

    def parse(self, response):
        item = a55dyItem()
        item['name'] = '55cc电影网'
        item['title'] = response.css('title::text').get()
        item['img'] = response.css('img::attr(src)').get()
        item['url'] = response.url
        yield item
        urls = response.css('a::attr(href)').getall()
        for url in urls:
            url = response.urljoin(url)
            yield scrapy.Request(url,callback=self.parse)
        pass
