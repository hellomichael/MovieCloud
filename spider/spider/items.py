# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose,TakeFirst

def repleaceSpace(str):
    str = str.replace('    ','')
    return str.replace('\n', '')

class SpiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

# 27k电影
class kSpiderItem(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field(input_processor=MapCompose(repleaceSpace), output_processor=TakeFirst())
    title = scrapy.Field(input_processor=MapCompose(repleaceSpace), output_processor=TakeFirst())
    url = scrapy.Field(input_processor=MapCompose(repleaceSpace), output_processor=TakeFirst())
    img = scrapy.Field(input_processor=MapCompose(repleaceSpace), output_processor=TakeFirst())
    pass

class Cmdy5Item(scrapy.Item):
    name = scrapy.Field()
    title = scrapy.Field()
    img = scrapy.Field()
    url = scrapy.Field()
