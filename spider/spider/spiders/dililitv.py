# -*- coding: utf-8 -*-
import scrapy

from scrapy.loader import ItemLoader

class dililitvSpider(scrapy.Spider):
    name = 'dilidili'
    allowed_domains = ['www.dililitv.com']
    start_urls = ['https://www.dililitv.com/']

    def parse(self, response):
        loader = ItemLoader(item=dililitvSpider(), response=response)
        loader.add_value('name','dilidili')
        loader.add_css('title', "body > section > div > div > div > div.m-movies.clearfix > article:nth-child(1) > a > h2::text")
        loader.add_css("url", "body > section > div > div > div > div.m-movies.clearfix > article:nth-child(1) > a::attr(href)")
        loader.add_css('img', "body > section > div > div > div > div.m-movies.clearfix > article:nth-child(1) > a > div > img::attr(src)")
        yield loader.load_item()
        urls = response.css('a::attr(href)').getall()
        for url in urls:
            url = response.urljoin(url)
            yield scrapy.Request(url, callback=self.parse)
        pass