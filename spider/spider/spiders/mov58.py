# -*- coding: utf-8 -*-
import scrapy

from jdjmoviespider.items import JdjmoviespiderItem


class Mov58Spider(scrapy.Spider):
    name = 'mov58'
    allowed_domains = ['58mov.com']
    start_urls = ['http://www.58mov.com/index.php/vod/show/id/1/page/4.html','http://www.58mov.com/index.php/vod/show/id/1/page/1.html','http://www.58mov.com/index.php/vod/show/id/1/page/2.html','http://www.58mov.com/index.php/vod/show/id/1/page/3.html']

    def parse(self, response):
        mov58_list = response.xpath("//div[@class='fed-part-layout fed-back-whits']//ul[@class='fed-list-info fed-part-rows']/li")
        for i_item in mov58_list:
            mov58_item = JdjmoviespiderItem()
            mov58_item['movie_source'] = '58mov'
            mov58_item['movie_name'] = i_item.xpath(".//a[@class='fed-list-title fed-font-xiv fed-text-center fed-text-sm-left fed-visible fed-part-eone']/text()").extract_first()
            mov58_item['movie_url'] = 'http://58mov.com'+i_item.xpath(".//a[@class='fed-list-title fed-font-xiv fed-text-center fed-text-sm-left fed-visible fed-part-eone']/@href").extract_first()
            mov58_item['movie_img'] = i_item.xpath(".//a[@class='fed-list-pics fed-lazy fed-part-2by3']/@data-original").extract_first()
            mov58_item['movie_desc'] = i_item.xpath(".//span[@class='fed-list-desc fed-font-xii fed-visible fed-part-eone fed-text-muted fed-hide-xs fed-show-sm-block']/text()").extract_first()
            yield mov58_item
        next_link = response.xpath("//div[@class='fed-page-info fed-text-center']/a[11]/@href").extract()
        if next_link:
            next_link = next_link[0]
            yield scrapy.Request('http://58mov.com' + next_link, callback=self.parse)
