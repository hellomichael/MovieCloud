# -*- coding: utf-8 -*-
import scrapy

from jdjmoviespider.items import JdjmoviespiderItem


class V7192Spider(scrapy.Spider):
    name = 'v7192'
    allowed_domains = ['v.7192.com']
    start_urls = ['http://v.7192.com/movie_0_0_0_0_1']

    def parse(self, response):
        movie_list_three = response.xpath('//div[@class="result_right_list"]/ul[@class="mv_list_ul"]/li')
        for i_item in movie_list_three:
            v7192_item = JdjmoviespiderItem()
            print(22323232323)
            v7192_item['movie_source'] = 'v9192'
            v7192_item['movie_name'] = i_item.xpath('./h3/text()').extract_first()
            v7192_item['movie_url'] = 'http://v.7192.com' + i_item.xpath('.//a/@href').extract_first()
            v7192_item['movie_img'] = i_item.xpath('.//img/@data-original').extract_first()
            v7192_item['movie_desc'] = i_item.xpath('./h3/text()').extract_first()
            print(v7192_item)
            yield  v7192_item
        next_link = response.xpath('.//div[@class="pages"]/a[8]/@href').extract()
        if next_link:
            next_link = next_link[0]
            yield scrapy.Request('http://v.7192.com/' + next_link, callback=self.parse)

