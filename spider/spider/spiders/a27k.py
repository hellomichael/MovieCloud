# -*- coding: utf-8 -*-
import scrapy

from ..items import kSpiderItem
from scrapy.loader import ItemLoader

class A27kSpider(scrapy.Spider):
    name = '27k'
    allowed_domains = ['www.27k.cc']
    start_urls = ['http://www.27k.cc/']

    def parse(self, response):
        loader = ItemLoader(item=kSpiderItem(), response=response)
        loader.add_value('name','27k')
        loader.add_css('title', "#movie_name::text")
        loader.add_css("url", "#mainbody > div.movie-info.col-lg-7.col-md-7.col-sm-7.col-xs-12 > a::attr(href)")
        loader.add_css('img', "#poster > a > img::attr(src)")
        yield loader.load_item()
        urls = response.css('a::attr(href)').getall()
        for url in urls:
            url = response.urljoin(url)
            yield scrapy.Request(url, callback=self.parse)
        pass
