# -*- coding: utf-8 -*-
import scrapy

from jdjmoviespider.items import JdjmoviespiderItem


class DiediaoSpiderSpider(scrapy.Spider):
    name = 'diediao_spider'
    allowed_domains = ['diediao123.com']
    start_urls = ['http://diediao123.com/dongzuopian/index.html',
                  'http://www.diediao123.com/xijupian/index.html',
                  'http://www.diediao123.com/kehuanpian/index.html',
                  'http://www.diediao123.com/kongbupian/index.html',
                  'http://www.diediao123.com/juqingpian/index.html',
                  'http://www.diediao123.com/aiqingpian/index.html',
                  'http://www.diediao123.com/zhanzheng/index.html',
                  'http://www.diediao123.com/jilupian/index.html',
                  'http://www.diediao123.com/weidianying/index.html'
                  ]

    def parse(self, response):
        movie_list = response.xpath('//div[@class="mod_video_list poster"]/div/ul/li')
        print(movie_list)
        for i_item in movie_list:
            diediao_item = JdjmoviespiderItem()
            diediao_item['movie_source'] = 'diediao'
            diediao_item['movie_name'] = i_item.xpath(".//h6[@class='scores']/a/text()").extract_first()
            diediao_item['movie_url'] = i_item.xpath(".//h6[@class='scores']/a/@href").extract_first()
            diediao_item['movie_img'] = i_item.xpath(".//a/img/@src").extract_first()
            diediao_item['movie_desc'] = i_item.xpath(".//h6[@class='scores']/a/text()").extract_first()
            yield diediao_item
        next_link = response.xpath(".//div[@class='mod_pagenav']/a[@title='下一页']/@href").extract()
        print(next_link)
        if next_link:
            next_link = next_link[0]
            yield scrapy.Request('http://www.diediao123.com' + next_link, callback=self.parse)
        pass
