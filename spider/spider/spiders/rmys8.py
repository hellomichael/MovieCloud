# -*- coding: utf-8 -*-

import scrapy
from scrapy.linkextractors import LinkExtractor

from spider.rmys8.parse_rmys8 import parse_detail


class RunMovieSpider2(scrapy.Spider):
    name = 'rmys8'
    allowed_domains = ['www.52dyy.vip', 'www.rmys8.com']
    start_urls = ['https://www.rmys8.com/']

    # 处理列表
    def parse(self, response):
        # 提取详情地址
        ext_detail = LinkExtractor(allow='detail', allow_domains=RunMovieSpider2.allowed_domains)
        links_detail = ext_detail.extract_links(response)
        for detail in links_detail:
            request = scrapy.Request(detail.url, callback=parse_detail)
            yield request
        # 爬取更多信息
        ext_list = LinkExtractor(deny='show')
        links_list = ext_list.extract_links(response)
        for listItem in links_list:
            request = scrapy.Request(listItem.url, callback=self.parse)
            yield request
