# -*- coding: utf-8 -*-
import scrapy

from spider.spider.items import kSpiderItem


class XL720Spider(scrapy.Spider):
    name = 'xl720'
    allowed_domains = ['https://www.xl720.com/']
    start_urls = ['https://www.xl720.com/category/dongzuopian/']

    def parse(self, response):

        content_list = response.xpath("//*[@class='post']")
        # post-40439 > div.entry-thumb-left > a > img
        for content in content_list:
            item = kSpiderItem()
            item["name"] = "xl720"
            item["image"] = content.xpath('.//div[1]/a/img/@src').extract_first()
            item["title"] = content.xpath('.//div[1]/a/img/@alt').extract_first()
            item["url"] = content.xpath('.//div[1]/a/@href').extract_first()
            yield item
        next_urls = response.xpath('//*[@id="content"]/div[3]/a')

        for next_url in next_urls:
            yield scrapy.Request(next_url.xpath('.//@href').extract_first(), callback=self.parse)
