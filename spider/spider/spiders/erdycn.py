# -*- coding: utf-8 -*-
import scrapy
from ..items import ErdyItem


class ErdycnSpider(scrapy.Spider):
    name = 'erdycn'
    allowed_domains = ['dianying.2345.com']
    start_urls = ['http://dianying.2345.com/']

    def parse(self, response):
        item = ErdyItem()
        item['name'] = '2345电影网'
        item['title'] = response.css('title::text').get()
        item['img'] = response.css('img::attr(src)').get()
        item['url'] = response.url
        yield item
        urls = response.css('a::attr(href)').getall()
        for url in urls:
            url = response.urljoin(url)
            yield scrapy.Request(url, callback=self.parse)
        pass
