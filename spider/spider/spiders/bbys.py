import scrapy

from jdjmoviespider.items import JdjmoviespiderItem


class DmozSpider(scrapy.Spider):
    name = 'bbys'
    allowed_domains = ['88ys.cc']
    start_urls = ['https://www.88ys.cc/vod-type-id-1-pg-4.html']

    def parse(self, response):
        movie_list = response.xpath("//div[@class='index-area clearfix']/ul/li")
        for i_item in movie_list:
            bbys_item = JdjmoviespiderItem()
            bbys_item['movie_source'] = '88ys'
            bbys_item['movie_name'] = i_item.xpath(
                ".//span[@class='lzbz']/p[@class='name']/text()").extract_first()
            bbys_item['movie_url'] = 'https://www.88ys.cc' + i_item.xpath(".//a/@href").extract_first()
            bbys_item['movie_img'] = i_item.xpath(".//a/img/@data-original").extract_first()
            bbys_item['movie_desc'] = i_item.xpath(
                ".//span[@class='lzbz']/p[2]/text()").extract_first()
            yield bbys_item
        next_link = response.xpath("//div[@class='page mb clearfix']/a[6]/@href").extract()
        print(next_link)
        if next_link:
            next_link = next_link[0]
            yield scrapy.Request('https://www.88ys.cc' + next_link, callback=self.parse)
