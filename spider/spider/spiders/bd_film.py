# -*- coding: utf-8 -*-
import scrapy
from spider.bd_film.bd_film_items import FilmItems as MyscrapyItem


class BdFilmSpider(scrapy.Spider):
    name = 'bd-film'
    allowed_domains = ['www.bd-film.cc']
    start_urls = ['http://www.bd-film.cc/']

    def parse(self, response):
        li_list = response.xpath("//div[@class='col-lg-6 padding-0']/li")
        for li in li_list:
            item = MyscrapyItem()
            name = li.xpath("./div/a/text()").extract_first()
            url = li.xpath("./div/a/@href").extract_first()
            print(name)
            print(url)
            yield item
