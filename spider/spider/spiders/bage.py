# -*- coding: utf-8 -*-
import scrapy
from ..items import Bage
class BageSpider(scrapy.Spider):
    name = 'bage'
    # allowed_domains = ['http://www.8gw.com/']
    start_urls = ['https://www.sm1234.cc/']

    def parse(self, response):
        item = Bage()
        item['url']   = response.url
        item['title'] = response.css('title::text').get()
        item['img'] = response.css('img::attr(src)').get()
        item['name'] = 'A4YY电影'
        yield item
        # title = response.css('title::text').get()
        # print(title)
        urls = response.css('a::attr(href)').getall()
        for url in urls:
            url = response.urljoin(url)
            yield scrapy.Request(url, callback=self.parse)

            # print(scrapy.Request(url, callback=self.parse))


