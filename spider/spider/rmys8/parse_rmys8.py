# 处理详情
from scrapy.loader import ItemLoader

from spider.rmys8.items_rmys8 import Rmys8MovieItem


def parse_detail(response):
    # title = response.css()[0].extract()
    item = ItemLoader(item=Rmys8MovieItem(), response=response)
    item.add_value('url', response.url)
    item.add_css('title', 'h1 a::text')
    item.add_css('img', '.fed-deta-images a::attr(data-original)')
    item.add_css('summary', '.fed-part-esan')
    esan = response.css('.fed-part-esan')
    # print(esan.get())

    # 解析其他信息
    lis = response.css('.fed-deta-content li')
    if len(lis) < 3:
        return None
    type = lis[2].css('a::text').get()
    item.add_value('category', type)

    region = lis[3].css('a::text').get()
    item.add_value('region', region)
    # item.add_css('category', '.fed-deta-content li:nth-child(5) a::text')
    # for li in lis:
    #     print(li.getall())
    #
    item = item.load_item()
    if item['title']:
        yield item
